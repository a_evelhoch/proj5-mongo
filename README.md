# Project 4: Brevet time calculator with Ajax

A RUSA ACP controle time calculator made to work with flask and ajax.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.

### A brief description of the algorithm

According to this table:
Distance to Controle Point (km) | Minimum Speed (kmph) | Maximum Speed (kmph)
--- | --- | ---
0-200 | 15 | 34
201-400 | 15 | 32
401-600 | 15 | 30
601-1000 | 11.428 | 28
1001-1300 | 13.333 | 26

A controle points opening time is based on the maximum speed in the table, and the closing time is based on the minimum speed. The way it is calculated is that for the distance to the controle point, each portion of the distance of the controle point is divided by the corresponding speed in the same row (minimum speed to determine closing time, maximum speed to determine opening time) and those are all summed up. For instance, an opening time for controle point at a distance of 750 would be 200/34 + 200/32 + 200/30 + 150/28. (24.1561162...). Once those are all summed up, taking the non-decimal portion of the total will give you the number of hours past the starting time that the controle pointe opens. (24 hours) To get the number of minutes, the non-decimal portion is removed, and the fractional portion is multiplied by 60 and rounded to the nearest whole number. (24 hours, 9 minutes)

### The relaxed closing time rule

Because using the simple algorith will cause any controle points less than 60km away from the start will end up with controle points that close before the starting point does, there is a rule to make them stay open longer. For Distances less than 60km, the minimum speed is treated as 20 kmph for the algorithm, and then 1 hour is added afterwards. This only applies if the controle point is less than 60km, you don't need to break the first Distance to controle point into 0-60 and 61-200. This version of the calculator uses logic that might not respect this rule.

## Running the calculator

To run the brevet time calculator, the first step will be to clone the repo to your machine. Then, navigate a terminal to the proj5-mongo/brevets/ folder, and use the "docker-compose up" command to start the containers up. Once the server is up and running, you can navigate to localhost:5000 in your browser to access the brevet time calculator. Once you've loaded up the calculator, enter in the starting date and time, and total brevet distance (optional). You can then enter in the distance to each controle point in either miles or kilometers, and the page will tell you what time the controle point should open and close. To shut down the server, go into the terminal window and press Ctrl+C.

### Saving your brevets

The calculator was enhanced to have the ability to save a brevet time. Once you've entered all your data in the table, press the "Submit brevet" button to save it in the database. You can then press the "Display saved brevet" button to pop up that data in a textbox. There is also a "Delete saved brevet" button, in case you want the database to forget the data you entered. This is not neccessary before submitting another brevet, since that will automatically replace the previous one.

## Testing

### Test cases

If you try to submit brevet to the database when there is no data in the table, it will fail with an error message. If you try to display the saved brevet when there is no saved brevet, it will fail with an error message. If you try to delete the saved brevet when there is no saved brevet, it will fail with an error message.

### Nose testing

A test_acp_times.py module is provided that works with pythons nose testing suite. It verifies that the algorithm correctly calculates the amount of time it should take for the controle points to open or close.

## Student Details

Name: Andrew Evelhoch
E-mail: aevelhoc@uoregon.edu
